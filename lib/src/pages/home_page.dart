import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas/src/providers/peliculas_provider.dart';
import 'package:peliculas/src/widgets/cart_swiper_widget.dart';
import 'package:peliculas/src/widgets/movie_horizontal.dart';


class HomePage extends StatelessWidget{
  final peliculasProvider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {
    peliculasProvider.getPopulares();
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigoAccent,
        title:Text('Peliculas en cine'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
            onPressed: (){},
          )
        ],
      ),
      body: Container(
        child: Column (
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
           _swiperTargetas(),
           _footer(context)
         ],
        )
      ),
    );
  }

  Widget _swiperTargetas() {


    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if(snapshot.hasData){
          return CardSwiper (peliculas: snapshot.data);
        } else {
          return Container(
              height: 200,
              child: Center(
                  child: CircularProgressIndicator()
              )
          );
        }
      }
    );
  }

  Widget _footer(context) {
    return Container(
      width: double.infinity,
      child: Column(

        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left:20),
            child:Text('Populares', style: Theme.of(context).textTheme.bodyText2),
          ),
          SizedBox(height: 2.0,),
          StreamBuilder(
            stream: peliculasProvider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (snapshot.hasData){
                return MovieHorizontal(
                    peliculas: snapshot.data,
                  siguientePagina: peliculasProvider.getPopulares,
                );
              } else {
                return Container();  
              }
              
            }
          )
        ],
      ),
    );
  }

}