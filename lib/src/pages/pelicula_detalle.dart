import 'package:flutter/material.dart';
import 'package:peliculas/src/models/pelicula_model.dart';



class PeliculaDetalle extends StatelessWidget {

//  final Pelicula pelicula;


  @override
  Widget build(BuildContext context) {

    final Pelicula pelicula = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar( pelicula ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox(height: 10,),
                _posterTitulo(pelicula),
                _descripcion(pelicula)
              ]
            ),
          )
        ],
      )
    );
  }

  Widget _crearAppbar(Pelicula pelicula) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: 200,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          pelicula.title,
          style: TextStyle(color:Colors.white,fontSize: 16.0)
        ),
      background: FadeInImage(
        image: NetworkImage(pelicula.getBackgroundImg()),
        placeholder: AssetImage('assets/img/Stalin.jpg'),
//        fadeInDuration: Duration(microseconds: 150),
        fit: BoxFit.cover
      ),
      ),
    );
  }

  Widget _posterTitulo(Pelicula pelicula) {
    return Container(
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child:Image(
              image: NetworkImage(pelicula.getPosterImg()),
              height: 150,
              ),
          ),
          SizedBox(width: 20.0,),
          Flexible(
            child: Column(
              children: <Widget>[
                Text(pelicula.title, style: TextStyle(fontSize: 20),overflow: TextOverflow.ellipsis),
                Text(pelicula.originalTitle, style: TextStyle(fontSize: 12),overflow: TextOverflow.ellipsis),
                Row (
                  children: <Widget>[
                    Icon(Icons.star_border),
                    Text(pelicula.voteAverage.toString())
                  ],
                )
              ],
            )
          )
        ],
      )
    );
  }

  Widget _descripcion(Pelicula pelicula) {

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 20.0),
      child: Text(
        pelicula.overview,
        textAlign: TextAlign.justify,
      ),

    );
  }




}