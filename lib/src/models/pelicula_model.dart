
class Peliculas {

  List<Pelicula> items = new List();

  Peliculas();

  Peliculas.fromJsonList( List<dynamic> jsonList ){

    if(jsonList == null) return;
    for(var item in jsonList) {
      final pelicula = new Pelicula.fromJsonMap(item);
      items.add(pelicula);
    }
  }


}

class Pelicula {

  int voteCount;
  int id;
  bool video;
  double voteAverage;
  String title;
  double popularity;
  String posterPath;
  String originalLanguage;
  String originalTitle;
  List<int> genreIds;
  String backdropPath;
  bool adult;
  String overview;
  String releaseDate;

  Pelicula({
    this.originalLanguage,
    this.adult,
    this.backdropPath,
    this.genreIds,
    this.id,
    this.originalTitle,
    this.overview,
    this.popularity,
    this.posterPath,
    this.releaseDate,
    this.title,
    this.video,
    this.voteAverage,
    this.voteCount
});

  Pelicula.fromJsonMap(Map<String, dynamic> json){
    originalLanguage  =json['original_language'];
    adult             =json['adult'];
    backdropPath      =json['backdrop_path'];
    genreIds          =json['genre_ids'].cast<int>();
    id                =json['id'];
    originalTitle     =json['original_title'];
    overview          =json['overview'];
    popularity        =json['popularity'] / 1;
    posterPath        =json['poster_path'];
    releaseDate       =json['release_date'];
    title             =json['title'];
    video             =json['video'];
    voteAverage       =json['vote_average'] / 1;
    voteCount         =json['vote_count'];
  }

  getPosterImg() {
    if(posterPath == null) {
      return 'http://4.bp.blogspot.com/-i22_w74nu_I/TqF8EUzI5OI/AAAAAAAAAHk/adPg6OjQMlU/s1600/Stalin.jpg';
    } else {
      return 'https://image.tmdb.org/t/p/original$posterPath';
    }

  }

  getBackgroundImg() {
    if(posterPath == null) {
      return 'http://4.bp.blogspot.com/-i22_w74nu_I/TqF8EUzI5OI/AAAAAAAAAHk/adPg6OjQMlU/s1600/Stalin.jpg';
    } else {
      return 'https://image.tmdb.org/t/p/original$backdropPath';
    }

  }
}