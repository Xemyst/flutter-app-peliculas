import 'package:flutter/material.dart';
import 'package:peliculas/src/routes/routes.dart';

void main() {
  runApp(Peliculas());
}

class Peliculas extends StatelessWidget {
  @override
  Widget build( BuildContext cotext ) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title:'Peliculas',
        initialRoute: 'home',
        routes: getRoutes()
    );
  }



}